const express = require('express');
const app = express();
const cors = require('cors');
const PORT = 8080;

app.use(cors());
app.use(express.json());

let brandList = [
    {
        id: 1,
        name: 'Iphone',
    },
    {
        id: 2,
        name: 'Samsung',
    },
    {
        id: 3,
        name: 'Nokia',
    },
];

app.get('/api/home', (req, res) => {
    res.json({
        brand: brandList,
    });
});

app.post('/api/home', (req, res) => {
    const newBrand = req.body;
    brandList.push(newBrand);

    res.json({
        success: true,
        message: 'Brand added successfully',
        brand: newBrand,
    });
});

app.listen(PORT, () => {
    console.log('Server started on port', PORT);
});